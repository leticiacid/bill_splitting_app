package com.example.bill_splitting_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculationPage extends AppCompatActivity {
    private String str;
    private Button end_button;
    private TextView initial_price;
    private EditText numPeople;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_page);
//        TextView tv_total = findViewById(R.id.tv_total);
        TextView tv_totalPrice = findViewById(R.id.total_cost);

        end_button = findViewById(R.id.end_button);
        initial_price = findViewById(R.id.initial_price);
        numPeople = findViewById(R.id.split_into);
        end_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(CalculationPage.this,EndPage.class);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        double total = intent.getDoubleExtra("total",1.02);
        str = Double.toString(total);
//
        tv_totalPrice.setText(str);
        initial_price.setText(str);


    }





    public void updatePrice(){



    }


}