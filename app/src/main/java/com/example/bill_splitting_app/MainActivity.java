package com.example.bill_splitting_app;

import static com.example.bill_splitting_app.OrderAdapter.mainActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;

public class MainActivity extends AppCompatActivity {

    //This code was used for the navigation bar. We no longer have a navigation bar.
//    @Override //inflates the menu/navigation bar with the xml file
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//    //add button functionality
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.back_button) {
//            System.out.println("You would navigate to the add screen here or call a function");
//        }
//        return super.onOptionsItemSelected(item);
//    }
    private OrderAdapter orderAdapter;
    private double total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        RecyclerView ordersRecyclerView = findViewById(R.id.recycler_view);
        ordersRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        orderAdapter = new OrderAdapter(Order.orders, getApplicationContext(), layoutInflater, this);
        ordersRecyclerView.setAdapter(orderAdapter);

        //Sets up the add order button
        ImageButton addOrderButton = findViewById(R.id.add_order_button);
        addOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order.orders.add(new Order("", 0.00, Order.orders.size()));
                int position = Order.orders.size() - 1;
                updateTotal();
                orderAdapter.notifyItemInserted(position);
            }
        });

        ImageButton splitBillButton = findViewById(R.id.split_bill_button);
        TextView incompleteError = findViewById(R.id.incomplete_error);
        TextView totalCost = findViewById(R.id.total_cost);
        //Split bill button. Checks if any of the orders are empty or if any of the prices are 0 or below
        splitBillButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < Order.orders.size(); i++) {
                    if (Order.orders.get(i).name.isEmpty() || Order.orders.get(i).price <= 0) {
                        incompleteError.setVisibility(View.VISIBLE);
                        totalCost.setVisibility(View.INVISIBLE);
                    } else {
                        incompleteError.setVisibility(View.INVISIBLE);
                        total = Order.totalPrice;
                        Intent intent = new Intent(MainActivity.this ,CalculationPage.class);
                        intent.putExtra("total",total);
                        startActivity(intent); // this is the moving part
                    }
                }
            }
        });

    }


    //This function sets up the popup and its buttons, and makes the popup appear
    public void putUpDeletePopup(int position, OrderAdapter.ViewHolder holder) {

        MaterialCardView deleteOrderPopup = findViewById(R.id.delete_order_popup);
        ImageButton deleteOrderYes = findViewById(R.id.delete_order_yes);
        ImageButton deleteOrderNo = findViewById(R.id.delete_order_no);
        TextView deleteOrderTextView = findViewById(R.id.delete_order_text_view);
        String positionString = String.valueOf(position + 1);
        deleteOrderTextView.setText("Delete Order " + positionString + "?");
        deleteOrderPopup.setVisibility(View.VISIBLE);

        deleteOrderYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order.orders.remove(position);
                orderAdapter.notifyItemRemoved(position);
                // Updates all the orders following the deleted one so that the order # is correct
                for (int i=position; i<Order.orders.size(); i++) {
                    orderAdapter.notifyItemChanged(i);
                }
                updateTotal();
                totalCostVisible();
                deleteOrderPopup.setVisibility(View.GONE);
            }
        });
        deleteOrderNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteOrderPopup.setVisibility(View.GONE);
            }
        });
    }

    //call to update the total cost whenever the total should be changed
    public void updateTotal() {
        TextView totalCost = findViewById(R.id.total_cost);
        Order.totalPrice = 0;
        for (int i = 0; i<Order.orders.size(); i++) {
            Order.totalPrice += Order.orders.get(i).price;
        }
        String totalCostString = String.valueOf(Order.totalPrice);
        totalCost.setText(totalCostString);
    }

    public void putAwayKeyboard() {
        View keyboardAwayView = findViewById(R.id.keyboard_away_view);
        keyboardAwayView.setVisibility(View.VISIBLE);
        keyboardAwayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                //if keyboard is showing
                if (imm.isAcceptingText()) {
                    // Dismiss keyboard
                    imm.hideSoftInputFromWindow(mainActivity.getCurrentFocus().getWindowToken(), 0);
                }
                keyboardAwayView.setVisibility(View.GONE);
            }
        });
    }
    //sets the total cost to visible and hides the error when you edit an order
    public void totalCostVisible() {
        TextView totalCost = findViewById(R.id.total_cost);
        TextView incompleteError = findViewById(R.id.incomplete_error);
        totalCost.setVisibility(View.VISIBLE);
        incompleteError.setVisibility(View.INVISIBLE);
    }
}