package com.example.bill_splitting_app;

import androidx.collection.ArraySet;

import java.util.ArrayList;

public class Order {

    String name;
    int orderNumber;
    double price = 0.00;

    static ArrayList<Order> orders = new ArrayList<>();
    static double totalPrice = 0.0;

    public Order(String name, double price, int orderNumber) {
        this.name = name;
        this.price = price;
        this.orderNumber = orderNumber;
    }
}
