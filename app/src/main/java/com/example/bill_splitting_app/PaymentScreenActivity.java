package com.example.bill_splitting_app;

import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;

public class PaymentScreenActivity extends AppCompatActivity {

    @Override //inflates the menu/navigation bar with the xml file
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.payment_screen_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

}