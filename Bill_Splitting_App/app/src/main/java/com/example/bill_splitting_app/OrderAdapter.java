package com.example.bill_splitting_app;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    // Constructor to move in data
    OrderAdapter(ArrayList<Order> orders, Context context, LayoutInflater layoutInflater, MainActivity mainActivity) {
        this.orders = orders;
        this.context = context;
        this.mainActivity = mainActivity;
    }

    private ArrayList<Order> orders;
    private Context context;
    static MainActivity mainActivity;


    @NonNull
    @Override
    //makes empty view holder
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.order_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
        //Alternatively,
        // View view = LayoutInflater.from(parent.getContext())
        //                .inflate(R.layout.item_order, parent, false);
        //        return new ViewHolder(view);
    }

    @Override //OrderAdapter.ViewHolder holder makes a view holder called holder. Also sets initial values for each item. Also enables the buttons.
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder holder, int position) {
        position = holder.getAdapterPosition();
        holder.order = orders.get(position);
        String positionString = String.valueOf(position + 1);
        String priceString = String.valueOf(holder.order.price);
        holder.orderNumber.setText("Order " + positionString);
        holder.orderName.setText(holder.order.name);
        holder.orderPrice.setText(priceString);
        holder.clearInfoButton.setVisibility(View.INVISIBLE);
        holder.orderDeleteButton.setVisibility(View.INVISIBLE);


        holder.orderOptionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //opens or closes options menu
                if (holder.clearInfoButton.getVisibility() == View.INVISIBLE && holder.orderDeleteButton.getVisibility() == View.INVISIBLE) {
                    holder.clearInfoButton.setVisibility(View.VISIBLE);
                    holder.orderDeleteButton.setVisibility(View.VISIBLE);
                } else {
                    holder.clearInfoButton.setVisibility(View.INVISIBLE);
                    holder.orderDeleteButton.setVisibility(View.INVISIBLE);
                }
            }
        });
        //clear info button
        holder.clearInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.orderName.setText("");
                holder.orderPrice.setText("0.0");
            }
        });
        //delete button
        holder.orderDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.putUpDeletePopup(holder.getAdapterPosition(), holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Order.orders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView orderNumber;
        TextView orderName;
        TextView orderPrice;
        ImageButton orderOptionsButton;
        ImageButton clearInfoButton;
        ImageButton orderDeleteButton;

        Order order;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
             orderNumber = itemView.findViewById(R.id.order_number);
             orderName = itemView.findViewById(R.id.order_name);
             orderPrice = itemView.findViewById(R.id.order_price);
             orderOptionsButton = itemView.findViewById(R.id.order_options_button);
             clearInfoButton = itemView.findViewById(R.id.clear_info_button);
             orderDeleteButton = itemView.findViewById(R.id.order_delete_button);

             //updates the array list when you edit one of the orders.
             orderName.addTextChangedListener(new TextWatcher() {
                 @Override
                 public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                 @Override
                 public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                 @Override
                 public void afterTextChanged(Editable editable) {
                     if (order != null) {
                         mainActivity.totalCostVisible();
                         String newName = orderName.getText().toString();
                         order.name = newName;
                     }
                 }
             });
             orderPrice.addTextChangedListener(new TextWatcher() {
                 @Override
                 public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                 }

                 @Override
                 public void onTextChanged(CharSequence s, int start, int before, int count) {

                 }

                 @Override
                 public void afterTextChanged(Editable s) {
                     if (order != null) {
                         mainActivity.totalCostVisible();
                         String newPrice = orderPrice.getText().toString();
                         if (!newPrice.isEmpty()) {
                             double priceDouble = Double.valueOf(newPrice);
                             order.price = priceDouble;
                             mainActivity.updateTotal();
                         }
                     }
                 }
             });
            //checks if you have selected a new edit text. Pulls up the keyboard remover if you have
             orderName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                 @Override
                 public void onFocusChange(View view, boolean hasFocus) {
                     if (hasFocus) {
                         mainActivity.putAwayKeyboard();
                     }
                 }
             });
             orderPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                 @Override
                 public void onFocusChange(View view, boolean hasFocus) {
                     if (hasFocus) {
                         mainActivity.putAwayKeyboard();
                     }
                 }
             });

        }
    }
}
